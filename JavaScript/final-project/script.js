class Model {
  constructor() {
    this.dinoElement = document.getElementById("dino");
    this.cactusElement = document.getElementById("cactus");
    this.finalScreen = document.getElementById("final");
    this.started = false;
    this.paused = true;
    this.isAlive = null;
    this.hasPassedCactus = false;
    this.score = 0;
  }

  bindGameChanged(callback) {
    this.onGameChanged = callback;
  }

  bindFinalResult(callback) {
    this.finalResult = callback;
  }

  jumpDino() {
    if (!this.dinoElement.classList.contains("jump")) {
      this.dinoElement.classList.add("jump");
      setTimeout(() => {
        this.dinoElement.classList.remove("jump");
        this.checkCollision();
      }, 300);    
    } 
  }

  checkCollision() {
    let dinoTop = parseInt(
      window.getComputedStyle(this.dinoElement).getPropertyValue("top")
    );
    let cactusLeft = parseInt(
      window.getComputedStyle(this.cactusElement).getPropertyValue("left")
    );
    if (cactusLeft < 35 && cactusLeft > 0 && dinoTop >= 150) {
      this.showFinal();      
    } else
    if (cactusLeft <= 35 && !this.hasPassedCactus) {
      this.incrementScore(); // увеличиваем счет только при успешном перепрыгивании
      this.hasPassedCactus = true;
    }
    if (cactusLeft <= -15) {
      this.hasPassedCactus = false;
    }
  }

  incrementScore() {
    this.score++;
    this.renderGame(this.score);
  }

  toggleStart() {
    this.started = !this.started;
    this.paused = !this.paused;
    if (this.started && !this.paused) {
      this.cactusElement.classList.add("cactus__animation");
    }
  }

  togglePause() {
    this.paused = !this.paused;
    this.started = !this.started;
    if (this.paused) {
      this.cactusElement.classList.remove("cactus__animation");
    }
  }

  resetGame() {   
    this.togglePause();   
    this.isAlive = null;
    this.hasPassedCactus = false;
    this.score = 0;
    this.renderGame(this.score);
  }

  showFinal(){
    this.togglePause();
    this.finalScreen.classList.remove("game__final");
  }

  hideFinal(){
     this.finalScreen.classList.add("game__final");
  }

  renderGame(score) {
    this.onGameChanged(score);
    this.finalResult(score);
  }
  
}

class View {
  constructor() {
    this.app = this.getElement(".game");
    this.finalModal = this.getElement(".game__content");

    // создем элементы на страниице
    this.buttonStart = this.createElement("button", "game__startButton");
    this.buttonPause = this.createElement("button", "game__button");
    this.imageGameStart = this.createElement("img", "game__start");
    this.imageGamePause = this.createElement("img", "game__pause");
    this.gameScore = this.createElement("div", "scoresheet");
    this.wrapperControl = this.createElement("div", "wrapper");
    this.buttonRestart = this.createElement("button", "game__restartButton");
    this.imageRestart = this.createElement("img", "game__restart");
    this.scoreSheet = this.createElement("div", "game__score");
    this.buttonFianlRestart = this.createElement("button", "game__restartButton");
    this.imageFinalRestart = this.createElement("img", "game__restart");

    this.buttonStart.append(this.imageGameStart);
    this.buttonPause.append(this.imageGamePause);
    this.buttonRestart.append(this.imageRestart);
    this.buttonFianlRestart.append(this.imageFinalRestart);

    this.wrapperControl.append(
      this.gameScore,
      this.buttonStart,
      this.buttonPause,
      this.buttonRestart
    );

    this.finalModal.append(
      this.scoreSheet,
      this.buttonFianlRestart  
    );

    //точка входа приложения
    this.app.append(this.wrapperControl);

    //Add content
    this.imageGameStart.src = "images/Start.png";
    this.imageGamePause.src = "images/Pause.png";
    this.imageRestart.src = "images/Restart.png";
    this.imageFinalRestart.src = "images/Restart.png";    
  }

  bindJumpDino(callback) {
    document.addEventListener("keydown", (event) => {
      if (event.key === " ") {
        callback();
      }
    });
  }

  bindToggleStart(callback) {
    this.buttonStart.addEventListener("click", () => {
      callback();
    });
  }

  bindTogglePause(callback) {
    this.buttonPause.addEventListener("click", () => {
      callback();
    });
  }

  bindRestart(callback) {
    this.buttonRestart.addEventListener("click", callback);
  }

  bindFinalRestart(callback) {
    this.buttonFianlRestart.addEventListener("click", callback);
  }

  updateScore(score) {
    const scoreElement = this.gameScore;
    scoreElement.textContent = `Score: ${score}`;
  }

  finalScore(score) {
    const finalScoreElement = this.scoreSheet;
    finalScoreElement.textContent = `Your Score: ${score}`;
  }

  getElement(selector) {
    const element = document.querySelector(selector);
    return element;
  }

  createElement(tag, className) {
    const element = document.createElement(tag);
    if (className) element.classList.add(className);
    return element;
  }
}

class Controller {
  constructor(model, view) {
    this.model = model;
    this.view = view;
    this.view.bindJumpDino(this.handleJumpDino);
    this.view.bindToggleStart(this.handleToggleStart);
    this.view.bindTogglePause(this.handleTogglePause);
    this.model.bindGameChanged(this.onGameChanged);
    this.model.bindFinalResult(this.finalGameScore)
    this.lastFrameTime = 0;
    this.gameLoop(); 
    this.onGameChanged(this.model.score);
    this.finalGameScore(this.model.score);
    this.view.bindRestart(this.handleRestart);
    this.view.bindFinalRestart(this.handleFinalRestart);
  }

  gameLoop = (timestamp) => {
    const deltaTime = timestamp - this.lastFrameTime;
    if (deltaTime >= 10) {
      this.model.checkCollision();
      this.lastFrameTime = timestamp;
    }
    requestAnimationFrame(this.gameLoop); 
  };

  handleJumpDino = () => {
    this.model.jumpDino();
  };

  onGameChanged = (score) => {
    this.view.updateScore(score);
  };

  handleToggleStart = () => {
    this.model.toggleStart();
  };

  handleTogglePause = () => {
    this.model.togglePause();
  };

  handleScore = () => {
    this.model.incrementScore();
  };

  handleRestart = () => {
    this.model.resetGame();
  };

  handleFinalRestart = () => {
    this.model.resetGame();
    this.model.hideFinal();
  };

  finalGameScore = (score)  => {
    this.view.finalScore(score-1);
  }
}
const app = new Controller(new Model(), new View());