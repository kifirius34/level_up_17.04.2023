'use strict'

//Задача 1
function summArr(arr){
  let summ = 0;
  let summ2 = 0;
  for(i=0; i<arr.length; i++) {
       if (typeof arr[i] == "number"){
         summ += arr[i];
       } else{
         summ2++
      }
     }
 if (summ2 !== arr.length){
   return summ;
 } else {
   return 0;
 }
}
console.log(summArr([1, 5.2, 4, 0, -1]))

//Задача 2
function shortcut(arr2) {
  return arr2.replace(/[a, e, i, o, u]/g, '');
}
console.log(shortcut('HELLO'))

//Задача 3
function volumeDiff(V1, V2){
  let v1 =1, v2=1;
  for(i=0; i<V1.length; i++) {
    v1 *= V1[i];
    v2 *= V2[i];
  }
  return Math.abs(v1-v2)
}
console.log(volumeDiff([2,2,3], [5,4,1]))
