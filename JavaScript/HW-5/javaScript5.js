'use strict'

//Задача 1

function isMyFavoriteHobby(element, index, array) {
  const myFavoriteHobby = 'смотреть сериальчики'
  return element === myFavoriteHobby
}

const currentToDoList = [
  'читать книгу',
  'пить кофе',
  'смотреть сериальчики',
  'гладить кота',
  'гулять',
]

 Array.prototype.myFind = function (callback, arg) {
  if (this === null || this === window) {
    throw TypeError('myFind called on null or undefiend');
  }

  if (typeof callback !== "function"){
    throw TypeError (`${callback} is not a function`);
  }

  for (let i = 0; i < this.length; i++) {
    let element = this[i]
    if (callback.call(arg, this[i], i, this)){
      return element;
    }
  }
}

console.log(currentToDoList.myFind(isMyFavoriteHobby))
 
//Задача 2

function minNumber (arr){
  let minNumber = arr[0];
  for (let i = 0; i < arr.length; i++) {
    if (minNumber > arr[i]) {
      minNumber = arr[i];
    }
  }
  return minNumber;
}

console.log(minNumber([34, 15, 88, 2, -2]));

//Задача 3

function sredArifm (arr){
  let sum=0;
  for (let i = 0; i < arr.length; i++) {
    sum += arr[i]
  }
  return (sum/arr.length);
}

console.log(sredArifm([1, 3, 5, 7, 4]));


//Задача 4

function sumMix (arr){
  let sum=0;
  for (let i = 0; i < arr.length; i++) {
    sum += Number(arr[i])
  }
  return sum;
}

console.log(sumMix([9, 3, '7', '3']));
