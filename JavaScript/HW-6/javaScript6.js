'use strict'

//Задача 1

let urls = ['guest.json', 'user.json'];

let promise = Promise.resolve();

let results = [];

urls.forEach((url) => {
  promise = promise.then(
    fetch(url).then((res) => 
      res.json().then((res) =>{
        results.push(res);
      })
    )
  );
} )

promise.then(() => {
  console.log(results)
})

//Задача 2

const promise1 = new Promise(function (resolve, reject) {
  setTimeout(() => {
      resolve("Result 1")
  }, 1000)
})

const promise2 = new Promise(function (resolve, reject) {
  setTimeout(() => {
      reject("Error1")
  }, 2000)
})

const promise3 = new Promise(function (resolve, reject) {
  setTimeout(() => {
      resolve("Result 2")
  }, 3000)
})

Promise.myall = function (values) {
  const promise = new Promise(function (resolve, reject) {
      let result = [];
      let total = 0;
      values.forEach((item, index) => {
          Promise.resolve(item).then((res) => {
              result[index] = res;
              total++;
              if (total === values.length)
                  resolve(result);
          }).
              catch((err) => {
                  reject(err);
              })
      })
  })
  return promise
}

Promise.myall([
  promise1,
  promise2,
  promise3
])
  .then((res) => {
      console.log(res);
  })
  .catch((er) => {
      console.log(er)
  })

Promise.myall([
  promise1,
  promise3,
])
  .then((res) => {
      console.log(res);
  })
  .catch((er) => {
      console.log(er)
  })