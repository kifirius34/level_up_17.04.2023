import React, {useId} from 'react';
import { List, Avatar,Tag } from "antd";
import { UserOutlined } from '@ant-design/icons';

function generateRandomUsers(count: number) {
  const users = [];
  
  for (let i = 0; i < count; i++) {
    const name = getRandomName();
    const age = getRandomAge();
    
    users.push({ name, age });
  }
  
  return users;
}

function getRandomName() {
  const names = ['John', 'Jane', 'Michael', 'Emma', 'David', 'Olivia', 'Daniel', 'Sophia', 'Иван', 'Игорь', 'Андрей', 'Давид', 'Юрий', 'Кирилл', 'Эдуард', 'Стас', 'Виктория', 'Александра'];
  return names[Math.floor(Math.random() * names.length)];
}

function getRandomAge() {
  return Math.floor(Math.random() * 99) + 1;
}

function UserList() {
  const users = generateRandomUsers(100);

  const id = useId();
  
  return (
    <List
    dataSource={users}
    renderItem={(user, index) => (
      <List.Item key={id}>
        <Avatar size={64} icon={<UserOutlined />} />
        <p>{user.name}, {user.age}</p>
        {index % 2 === 0 ? (
              <Tag color="red">Четный</Tag>
            ) : (
              <Tag color="green">Нечетный</Tag>
            )} 
      </List.Item>
    )}
    />
    );
    }
    
    export default UserList;